﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductFeedApp
{
	public class BuzzyBeeDept
	{
		public int DeptID { get; set; }
		public string DeptThaiName { get; set; }
		public string DeptEngName { get; set; }
		public int DeptIDLayer2 { get; set; }
		public string DeptThaiNameLayer2 { get; set; }
		public string DeptEngNameLayer2 { get; set; }
		public string CodeId { get; set; }
	}
}
