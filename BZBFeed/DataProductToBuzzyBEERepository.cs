﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using OfficeMate.Framework;
using System.Text.RegularExpressions;

namespace ProductFeedApp
{
	class DataProductToBuzzyBEERepository
	{
		public SqlConnection connection { get; set; }
		public DataProductToBuzzyBEERepository(SqlConnection _connection) { connection = _connection; }

		public DataTable GetData()
		{
			DataTable dt = new DataTable();
			using (SqlCommand cmd = new SqlCommand(@"SELECT pID,pTName,pEName,C.DeptID,ParentDeptId as SubDeptID,pTSDesc,pESDesc,pTLDesc,pELDesc,Status,
pTUnit,pEUnit,PromotionTtext,PromotionEtext,FullPrice,PriceIncVat,PriceExcVat,C.BrandID,BrandName,
DeliverFee,IsVat,IsBestDeal,IsPremium,pInstallment,IsPromotion,C.startdate as PromoStartdate,
C.enddate as PromoEnddate,barcode,Incatalog,QtyInstock,C.CreateOn,C.CreateBy,C.UpdateOn,C.UpdateBy 
FROM TBProductCenter C WITH (NOLOCK)
	--INNER JOIN TBBrandmaster brand WITH (NOLOCK) ON C.BrandID = brand.BrandID 
	INNER JOIN TBDeptMaster dept WITH (NOLOCK) ON C.DeptID = dept.DeptID 
	INNER JOIN TBDeptStructure deptStr WITH (NOLOCK) ON dept.DeptID = deptStr.DeptId AND deptStr.DeptStatus='Active' 
	--INNER JOIN TBDeptMaster parent WITH (NOLOCK) ON deptStr.ParentDeptId = parent.DeptID
WHERE Status NOT IN ( 'Delete', 'Hold', 'Clearance' )
order by pID"))
			{
				using (SqlDataAdapter sda = new SqlDataAdapter())
				{
					cmd.Connection = connection;
					sda.SelectCommand = cmd;


					sda.Fill(dt);



				}
			}
			return dt;
		}

		public List<BuzzyBEE> GetBuzzyBEE()
		{
			List<BuzzyBEE> BB = new List<BuzzyBEE>();

			using (SqlCommand command = new SqlCommand(@"
DECLARE @ratepoint INT
DECLARE @minpoint DECIMAL
DECLARE @maxpoint DECIMAL
--SELECT TOP 1 @ratepoint = RedeemPointRate,@minpoint = MinPoint ,@maxpoint = MaxPoint   FROM dbo.TBMaintainT1CUse WHERE ValidTo > GETDATE() and Status = 'Active'
Set @ratepoint = 8

TRUNCATE table TBProductfeeDailyBZB
	INSERT  INTO TBProductfeeDailyBZB


		SELECT		C.pID
					,pTName,pEName
					,C.DeptID,ParentDeptId as SubDeptID
					,pTSDesc,pESDesc,pTLDesc,pELDesc,Status
					,pTUnit,pEUnit,PromotionTtext,PromotionEtext
					,FullPrice
					,CASE 
							WHEN (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0)   THEN 
									ISNULL(exclus.PromotionPriceIncVat,0)
									ELSE PriceIncVat
					END	 AS PriceIncVat 
					,CASE 
							WHEN (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0)   THEN 
									ISNULL(exclus.PromotionPrice,0)
									ELSE PriceExcVat
					END	 AS PriceExcVat 
					,C.BrandID
					,c.BrandName,ISNULL(brand.pBrandEng,'') AS BrandEName
					,DeliverFee,IsVat,IsBestDeal,IsPremium,'No' AS pInstallment
					,CASE 
							WHEN (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0)   THEN 'No'  
							ELSE IsPromotion
					END	 AS IsPromotion 
					,barcode,Incatalog,QtyInstock
					,CASE 
							WHEN (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0)   THEN 'Yes'  
							ELSE 'No'
					END	 AS IsExclusive ,
					CASE 
							WHEN (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0)   THEN ISNULL(exclus.PromotionPriceIncVat,0) *  isnull(@ratepoint,8)
							ELSE PriceIncVat  *  isnull(@ratepoint,8)
					END	 AS T1CPointForProduct,
					CASE 
							WHEN C.Status = 'Clearance' THEN 'Yes'  
							ELSE 'No'
					END	 AS IsClearance,
					CASE
							WHEN exclus.CampaignType ='FlashDeal' THEN 'Yes' ELSE 'No'
					END as IsFlashDeal ,
					CASE
							WHEN exclus.CampaignType ='FlashDeal' AND (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0) THEN exclus.CampaignFrom  ELSE ''
					END as FlashDealStartDate,
					CASE
							WHEN exclus.CampaignType ='FlashDeal' AND (DATEDIFF(day,exclus.CampaignFrom,GETDATE()) >= 0  AND DATEDIFF(day,exclus.CampaignTo,GETDATE()) <= 0) THEN exclus.CampaignTo  ELSE ''
					END AS	FlashDealEndDate,
					img.img1 AS ImageURL1 ,
					img.img2 AS ImageURL2 ,
					img.img3 AS ImageURL3 ,
					img.img4 AS ImageURL4 ,
					img.img5 AS ImageURL5 ,
					img.img6 AS ImageURL6 ,
					img.img7 AS ImageURL7 ,
					img.img8 AS ImageURL8 ,
					img.img9 AS ImageURL9 ,
					img.img10 AS ImageURL10,
					'' as Type
						,C.CodeId, C.keyword, dept.DeptThaiName, dept.DeptEngName,
						ISNULL(DeptNameLevel1, '') as DeptNameLevel1, ISNULL(DeptNameLevel2, '') as DeptNameLevel2,
						ISNULL(DeptNameLevel3, '') as DeptNameLevel3, ISNULL(DeptNameLevel4, '') as DeptNameLevel4,'No','No',
						ISNULL(D.DeptNameEngLevel1, '') as DeptNameEngLevel1 , ISNULL(D.DeptNameEngLevel2, '') as DeptNameEngLevel2
		FROM TBProductCenter C WITH (NOLOCK)
			INNER JOIN
						(
											SELECT Pid
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' + pid+REPLACE([0],0,'.jpg'),'')AS img0
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' + pid+REPLACE([1],1,'.jpg'),'')AS img1
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[2]) + '.jpg','') AS img2
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[3]) + '.jpg','') AS img3
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[4]) + '.jpg','') AS img4
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[5]) + '.jpg','') AS img5
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[6]) + '.jpg','') AS img6
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[7]) + '.jpg','') AS img7
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[8]) + '.jpg','') AS img8
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[9]) + '.jpg','') AS img9
								,ISNULL('http://cdn'+CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1)+'.officemate.co.th/images/lpimage/' +pid+'_X'+CONVERT(VARCHAR(2),[10]) + '.jpg','') AS img10
	

							from
							(
								SELECT  t1.PID,ROW_NUMBER() OVER(PARTITION BY t1.pid ORDER BY imageid) AS Seq FROM dbo.TBProductImages t1 INNER JOIN   TBProductCenter t2 ON t1.Pid = t2.pID 
							) d
							pivot
							(
								max(Seq)
								for Seq in ([0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10])
							) piv
						)img ON img.Pid = C.pID
			INNER JOIN TBDeptMaster dept WITH (NOLOCK) ON C.DeptID = dept.DeptID 
			INNER JOIN TBDeptStructure deptStr WITH (NOLOCK) ON dept.DeptID = deptStr.DeptId AND deptStr.DeptStatus='Active'  AND deptStr.ISdefault = 'Yes'
			LEFT JOIN  VI_ExclusivePriceOnline exclus WITH (NOLOCK) ON exclus.PID = C.pID
			LEFT JOIN dbo.TBBrandmaster brand WITH (NOLOCK) ON brand.BrandID  = c.BrandID
			LEFT Join TBParentDeptName D  WITH ( NOLOCK ) On C.DeptId = D.DeptId
				--WHERE 
				--Status NOT IN ( 'Delete', 'Hold' , 'Clearance')  AND 
				--c.pID  NOT IN ( SELECT ProductId  FROM  TBGiftPremiumGroup) and  
				--c.DeptID NOT IN (SELECT  DeptId FROM dbo.TBDeptStructure   WHERE ParentDeptId ='2863')
				--order by c.pID

		UPDATE
			Table_A
		SET
			Table_A.Status = 'OutOfStock',
			Table_A.IsNotOrder = 'Yes'
		FROM
		TBProductfeeDailyBZB AS Table_A
		INNER JOIN TBProductCenter AS Table_B
        ON Table_A.pID = Table_B.pID
		WHERE
		Table_B.pCatID='A'AND Table_B.pSubCatID = '56' AND (Table_B.DeptID <> 2881 AND Table_B.BrandId <> 'B0556' AND Table_B.BrandId <> 'B1063')


		UPDATE
			Table_A
		SET
			Table_A.IsStoreExclude = 'Yes'
		FROM
		TBProductfeeDailyBZB AS Table_A
		INNER JOIN TBStoreReceive_Exclude AS Table_B
        ON Table_A.pID = Table_B.pID AND Table_B.Status ='Active'
	

		UPDATE
			Table_A
		SET
			Table_A.ImageURL2 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 2
				AND   SUBSTRING(Table_A.ImageURL2,45,LEN(Table_A.ImageURL2)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL3 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 3
				AND  SUBSTRING(Table_A.ImageURL3,45,LEN(Table_A.ImageURL3)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL4 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 4
				AND  SUBSTRING(Table_A.ImageURL4,45,LEN(Table_A.ImageURL4)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL5 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 5
				AND  SUBSTRING(Table_A.ImageURL5,45,LEN(Table_A.ImageURL5)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL6 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 6
				AND  SUBSTRING(Table_A.ImageURL6,45,LEN(Table_A.ImageURL6)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL7 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 7
				AND  SUBSTRING(Table_A.ImageURL7,45,LEN(Table_A.ImageURL7)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL8 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 8
				AND  SUBSTRING(Table_A.ImageURL8,45,LEN(Table_A.ImageURL8)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL9 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 9
				AND  SUBSTRING(Table_A.ImageURL9,45,LEN(Table_A.ImageURL9)) <> Table_B.ImageName
		UPDATE
			Table_A
		SET
			Table_A.ImageURL10 = ISNULL('http://cdn'+ CONVERT(VARCHAR(2), ABS(Checksum(NewID()) % 8) + 1),'')+'.officemate.co.th/images/lpimage/'+Table_B.ImageName
		FROM
			TBProductfeeDailyBZB AS Table_A INNER JOIN TBProductImages AS Table_B ON Table_A.pID = Table_B.pID 
				AND Table_B.Seq = 10
				AND  SUBSTRING(Table_A.ImageURL10,45,LEN(Table_A.ImageURL10)) <> Table_B.ImageName


	TRUNCATE table TBProductfeedBZB


		-- เคส 1  สินค้า New เจอใน TBProductfeeDailyBZB แต่ไม่เจอใน  init
		--BEGIN TRY 
		INSERT INTO TBProductfeedBZB
		SELECT pID,pTName,pEName,DeptID,SubDeptID,pTSDesc,pESDesc,pTLDesc,pELDesc,Status,pTUnit,pEUnit,PromotionTtext,PromotionEtext,FullPrice,PriceIncVat,PriceExcVat,BrandID,BrandName,BrandEName,DeliverFee,IsVat,IsBestDeal,IsPremium,pInstallment,IsPromotion
		,barcode,Incatalog,QtyInstock,IsExclusive,T1CPointForProduct,IsClearance,IsFlashDeal,FlashDealStartDate,FlashDealEndDate,ImageURL1,ImageURL2,ImageURL3,ImageURL4,ImageURL5,ImageURL6,ImageURL7,ImageURL8,ImageURL9,ImageURL10,'New',CodeId,IsNotOrder,IsStoreExclude,DeptNameLevel1,DeptNameEngLevel1,DeptNameLevel2,DeptNameEngLevel2 FROM TBProductfeeDailyBZB  WHERE pid  NOT IN (SELECT  pid FROM dbo.TBProductfeedBZBInit   )

		INSERT INTO TBProductfeedBZBInit
		SELECT pID,pTName,pEName,DeptID,SubDeptID,pTSDesc,pESDesc,pTLDesc,pELDesc,Status,pTUnit,pEUnit,PromotionTtext,PromotionEtext,FullPrice,PriceIncVat,PriceExcVat,BrandID,BrandName,BrandEName,DeliverFee,IsVat,IsBestDeal,IsPremium,pInstallment,IsPromotion
		,barcode,Incatalog,QtyInstock,IsExclusive,T1CPointForProduct,IsClearance,IsFlashDeal,FlashDealStartDate,FlashDealEndDate,ImageURL1,ImageURL2,ImageURL3,ImageURL4,ImageURL5,ImageURL6,ImageURL7,ImageURL8,ImageURL9,ImageURL10,'New',CodeId,IsNotOrder,IsStoreExclude,DeptNameLevel1,DeptNameEngLevel1,DeptNameLevel2,DeptNameEngLevel2 FROM TBProductfeeDailyBZB  WHERE pid  NOT IN (SELECT  pid FROM dbo.TBProductfeedBZBInit   )

		-- เคส 2 สินค้า Delete เจอใน init แต่ไม่เจอใน  TBProductfeeDailyBZB
		INSERT INTO TBProductfeedBZB
		SELECT pID,pTName,pEName,DeptID,SubDeptID,pTSDesc,pESDesc,pTLDesc,pELDesc,Status,pTUnit,pEUnit,PromotionTtext,PromotionEtext,FullPrice,PriceIncVat,PriceExcVat,BrandID,BrandName,BrandEName,DeliverFee,IsVat,IsBestDeal,IsPremium,pInstallment,IsPromotion
		,barcode,Incatalog,QtyInstock,IsExclusive,T1CPointForProduct,IsClearance,IsFlashDeal,FlashDealStartDate,FlashDealEndDate,ImageURL1,ImageURL2,ImageURL3,ImageURL4,ImageURL5,ImageURL6,ImageURL7,ImageURL8,ImageURL9,ImageURL10,'Delete',CodeId,IsNotOrder,IsStoreExclude,DeptThaiName,DeptNameEngLevel1,DeptThaiNameLavel2,DeptNameEngLevel2 FROM TBProductfeedBZBInit  WHERE pid  NOT IN (SELECT  pid FROM dbo.TBProductfeeDailyBZB   )

		-- เคส 3 สินค้า Update, (ราคาเปลี่ยน) ,(โปรโมชั่นไม่มี) ,(IsBestDeal)
		INSERT INTO TBProductfeedBZB
		SELECT T1.pID,T1.pTName,T1.pEName,T1.DeptID,T1.SubDeptID,T1.pTSDesc,T1.pESDesc,T1.pTLDesc,T1.pELDesc,T1.Status,T1.pTUnit,T1.pEUnit,T1.PromotionTtext,T1.PromotionEtext,T1.FullPrice,T1.PriceIncVat,T1.PriceExcVat,T1.BrandID,T1.BrandName,T1.BrandEName,T1.DeliverFee,T1.IsVat,T1.IsBestDeal,T1.IsPremium,T1.pInstallment,T1.IsPromotion
		,T1.barcode,T1.Incatalog,T1.QtyInstock,T1.IsExclusive,T1.T1CPointForProduct,T1.IsClearance,T1.IsFlashDeal,T1.FlashDealStartDate,T1.FlashDealEndDate,T1.ImageURL1,T1.ImageURL2,T1.ImageURL3,T1.ImageURL4,T1.ImageURL5,T1.ImageURL6,T1.ImageURL7,T1.ImageURL8,T1.ImageURL9,T1.ImageURL10,'Update',
		T1.CodeId,T1.IsNotOrder,T1.IsStoreExclude,T1.DeptNameLevel1,T1.DeptNameEngLevel1,T1.DeptNameLevel2,T1.DeptNameEngLevel2
		FROM 
					TBProductfeeDailyBZB T1 INNER JOIN TBProductfeedBZBInit T2 ON T1.pID = T2.pID 
		AND (
					(T1.PriceIncVat <> T2.PriceIncVat) 
				OR (T1.IsBestDeal <> T2.IsBestDeal) 
				OR (T1.PromotionTtext <> T2.PromotionTtext)
				OR (T1.T1CPointForProduct <> T2.T1CPointForProduct)
				OR (T1.DeptID <> T2.DeptID)
				OR (T1.SubDeptID <> T2.SubDeptID)
				OR (T1.Status <> T2.Status)
				OR	(T1.IsNotOrder <> T2.IsNotOrder)
				OR	(T1.CodeId <> T2.CodeId)
				OR	(T1.IsStoreExclude <> T2.IsStoreExclude)
				OR (T1.IsFlashDeal <> T2.IsFlashDeal )
				OR (T1.FlashDealStartDate <> T2.FlashDealStartDate)
				OR (T1.FlashDealEndDate <> T2.FlashDealEndDate)
				OR (SUBSTRING(T1.ImageURL1,45,LEN(T1.ImageURL1)) <>  SUBSTRING(T2.ImageURL1,45,LEN(T2.ImageURL1)))
				OR (SUBSTRING(T1.ImageURL2,45,LEN(T1.ImageURL2)) <>  SUBSTRING(T2.ImageURL2,45,LEN(T2.ImageURL2))) 
				OR (SUBSTRING(T1.ImageURL3,45,LEN(T1.ImageURL3)) <>  SUBSTRING(T2.ImageURL3,45,LEN(T2.ImageURL3))) 
				OR (SUBSTRING(T1.ImageURL4,45,LEN(T1.ImageURL4)) <>  SUBSTRING(T2.ImageURL4,45,LEN(T2.ImageURL4))) 
		        OR (SUBSTRING(T1.ImageURL5,45,LEN(T1.ImageURL5)) <>  SUBSTRING(T2.ImageURL5,45,LEN(T2.ImageURL5)))
				OR (SUBSTRING(T1.ImageURL6,45,LEN(T1.ImageURL6)) <>  SUBSTRING(T2.ImageURL6,45,LEN(T2.ImageURL6))) 
				OR (SUBSTRING(T1.ImageURL7,45,LEN(T1.ImageURL7)) <>  SUBSTRING(T2.ImageURL7,45,LEN(T2.ImageURL7))) 
				OR (SUBSTRING(T1.ImageURL8,45,LEN(T1.ImageURL8)) <>  SUBSTRING(T2.ImageURL8,45,LEN(T2.ImageURL8))) 
				OR (SUBSTRING(T1.ImageURL9,45,LEN(T1.ImageURL9)) <>  SUBSTRING(T2.ImageURL9,45,LEN(T2.ImageURL9)))
				OR (SUBSTRING(T1.ImageURL10,45,LEN(T1.ImageURL10)) <>  SUBSTRING(T2.ImageURL10,45,LEN(T2.ImageURL10)))  
			)

		UPDATE
			Table_A
		SET
			Table_A.PriceIncVat = Table_B.PriceIncVat,
			Table_A.PriceExcVat = Table_B.PriceExcVat,
			Table_A.IsBestDeal = Table_B.IsBestDeal,
			Table_A.PromotionTtext = Table_B.PromotionTtext,
			Table_A.T1CPointForProduct = Table_B.T1CPointForProduct,
			Table_A.DeptID = Table_B.DeptID,
			Table_A.SubDeptID = Table_B.SubDeptID,
			Table_A.Status = Table_B.Status,
			Table_A.ImageURL1 = Table_B.ImageURL1,
			Table_A.ImageURL2 = Table_B.ImageURL2,
			Table_A.ImageURL3 = Table_B.ImageURL3,
			Table_A.ImageURL4 = Table_B.ImageURL4,
			Table_A.ImageURL5 = Table_B.ImageURL5,
			Table_A.ImageURL6 = Table_B.ImageURL6,
			Table_A.ImageURL7 = Table_B.ImageURL7,
			Table_A.ImageURL8 = Table_B.ImageURL8,
			Table_A.ImageURL9 = Table_B.ImageURL9,
			Table_A.ImageURL10 = Table_B.ImageURL10,
			Table_A.CodeId = Table_B.CodeId,
			Table_A.IsNotOrder = Table_B.IsNotOrder,
			Table_A.IsStoreExclude = Table_B.IsStoreExclude, 
			Table_A.IsFlashDeal = Table_B.IsFlashDeal,
			Table_A.FlashDealStartDate = Table_B.FlashDealStartDate,
			Table_A.FlashDealEndDate = Table_B.FlashDealEndDate
		FROM
		TBProductfeedBZBInit AS Table_A
		INNER JOIN TBProductfeeDailyBZB AS Table_B
        ON Table_A.pID = Table_B.pID
		WHERE
		(Table_A.PriceIncVat <> Table_B.PriceIncVat)
		OR (Table_A.IsBestDeal <> Table_B.IsBestDeal) 
		OR (Table_A.PromotionTtext <> Table_B.PromotionTtext)
		OR (Table_A.DeptID <> Table_B.DeptID)
		OR (Table_A.SubDeptID <> Table_B.SubDeptID)
		OR (Table_A.Status <> Table_B.Status)
		OR (Table_A.T1CPointForProduct <> Table_B.T1CPointForProduct)
		OR (Table_A.IsNotOrder <> Table_B.IsNotOrder)
		OR (Table_A.CodeId <> Table_B.CodeId)
		OR (Table_A.IsStoreExclude <> Table_B.IsStoreExclude)
		OR (Table_A.IsFlashDeal <> Table_B.IsFlashDeal)
		OR (Table_A.FlashDealStartDate <> Table_B.FlashDealEndDate)
		OR (SUBSTRING(Table_A.ImageURL1,45,LEN(Table_A.ImageURL1)) <>  SUBSTRING(Table_B.ImageURL1,45,LEN(Table_B.ImageURL1)))
		OR (SUBSTRING(Table_A.ImageURL2,45,LEN(Table_A.ImageURL2)) <>  SUBSTRING(Table_B.ImageURL2,45,LEN(Table_B.ImageURL2)))
		OR (SUBSTRING(Table_A.ImageURL3,45,LEN(Table_A.ImageURL3)) <>  SUBSTRING(Table_B.ImageURL3,45,LEN(Table_B.ImageURL3)))
		OR (SUBSTRING(Table_A.ImageURL4,45,LEN(Table_A.ImageURL4)) <>  SUBSTRING(Table_B.ImageURL4,45,LEN(Table_B.ImageURL4)))
		OR (SUBSTRING(Table_A.ImageURL5,45,LEN(Table_A.ImageURL5)) <>  SUBSTRING(Table_B.ImageURL5,45,LEN(Table_B.ImageURL5)))
		OR (SUBSTRING(Table_A.ImageURL6,45,LEN(Table_A.ImageURL6)) <>  SUBSTRING(Table_B.ImageURL6,45,LEN(Table_B.ImageURL6)))
		OR (SUBSTRING(Table_A.ImageURL7,45,LEN(Table_A.ImageURL7)) <>  SUBSTRING(Table_B.ImageURL7,45,LEN(Table_B.ImageURL7)))
		OR (SUBSTRING(Table_A.ImageURL8,45,LEN(Table_A.ImageURL8)) <>  SUBSTRING(Table_B.ImageURL8,45,LEN(Table_B.ImageURL8)))
		OR (SUBSTRING(Table_A.ImageURL9,45,LEN(Table_A.ImageURL9)) <>  SUBSTRING(Table_B.ImageURL9,45,LEN(Table_B.ImageURL9)))
		OR (SUBSTRING(Table_A.ImageURL10,45,LEN(Table_A.ImageURL10)) <>  SUBSTRING(Table_B.ImageURL10,45,LEN(Table_B.ImageURL10)))
	
		--END TRY
		--	BEGIN CATCH PRINT ERROR_MESSAGE()
			--	END CATCH 

		UPDATE
			Table_A
		SET
			Table_A.Type = Table_B.Type
		FROM
			TBProductfeedBZBInit AS Table_A INNER JOIN TBProductfeedBZB AS Table_B ON Table_A.pID = Table_B.pID 


		Delete TBProductfeedBZBInit  Where Status  IN ( 'Delete', 'Hold' , 'Clearance') and  Type in ('New','Delete')
		Delete TBProductfeedBZB  Where Status  IN ( 'Delete', 'Hold' , 'Clearance') and  Type in ('New','Delete')

		UPDATE
			TBProductfeedBZBInit
		SET
			Type = 'Delete'
		Where Status  IN ( 'Delete', 'Hold' , 'Clearance')

		UPDATE
			TBProductfeedBZB
		SET
			Type = 'Delete'
		Where Status  IN ( 'Delete', 'Hold' , 'Clearance')

		UPDATE
			TBProductfeedBZBInit
		SET
			Type = 'Delete'
		Where pID  IN ( SELECT ProductId  FROM  TBGiftPremiumGroup)  Or DeptID  IN (SELECT  DeptId FROM dbo.TBDeptStructure   WHERE ParentDeptId ='2863')

		UPDATE
			TBProductfeedBZB
		SET
			Type = 'Delete'
		Where pID  IN ( SELECT ProductId  FROM  TBGiftPremiumGroup) Or DeptID  IN (SELECT  DeptId FROM dbo.TBDeptStructure   WHERE ParentDeptId ='2863')

		
		INSERT INTO TBParentDeptName
			SELECT distinct  deptStr.deptId, dept.deptThainame,dept.DeptEngName , deptMas.deptThainame ,deptMas.DeptEngName,'','','',getdate(),'surapon@officemate.co.th'
			FROM  TBDeptMaster dept 
				INNER JOIN TBDeptStructure deptStr  ON dept.DeptID = deptStr.ParentDeptId  AND deptStr.DeptStatus IN ('Active' ,'NonActive')  AND deptStr.ISdefault = 'Yes' 
				INNER JOIN  TBDeptMaster deptMas ON  deptMas.DeptID = deptStr.deptId
		WHERE  deptStr.deptId IN (SELECT DISTINCT deptId  FROM TBProductfeeDailyBZB WHERE DeptNameLevel1 = '')

		INSERT  TBProductfeedBZBLog 
		SELECT  *,'OFM',GETDATE() FROM dbo.TBProductfeedBZB
		
		SELECT   * FROM TBProductfeedBZB", connection))
			{
				command.CommandTimeout = 300;
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						BuzzyBEE data = new BuzzyBEE();
						data.pID = (string)reader["pID"];
						data.pTName = (string)reader["pTName"];
						data.pEName = (string)reader["pEName"];
						data.DeptID = reader["DeptID"].ToString();
						data.CodeId = reader["CodeId"].ToString();
						data.SubDeptID = reader["SubDeptID"].ToString();
						data.pTSDesc = (string)reader["pTSDesc"];
						data.pESDesc = (string)reader["pESDesc"];
						data.pTLDesc = (string)reader["pTLDesc"];
						data.pELDesc = (string)reader["pELDesc"];
						data.Status = (string)reader["Status"];
						data.pTUnit = (string)reader["pTUnit"];
						data.pEUnit = (string)reader["pEUnit"];
						data.PromotionTtext = (string)reader["PromotionTtext"];
						data.PromotionEtext = (string)reader["PromotionEtext"];
						data.FullPrice = (decimal)reader["FullPrice"];
						data.PriceIncVat = (decimal)reader["PriceIncVat"];
						data.PriceExcVat = (decimal)reader["PriceExcVat"];
						data.BrandID = (string)reader["BrandID"];
						data.BrandName = (string)reader["BrandName"];
						data.BrandEName = (string)reader["BrandEName"];
						data.DeliverFee = (decimal)reader["DeliverFee"];
						data.IsVat = (string)reader["IsVat"];
						data.IsBestDeal = (string)reader["IsBestDeal"];
						data.IsPremium = (string)reader["IsPremium"];
						data.pInstallment = (string)reader["pInstallment"];
						data.IsPromotion = (string)reader["IsPromotion"];
						data.barcode = (string)reader["barcode"];
						data.Incatalog = (string)reader["Incatalog"];
						data.QtyInstock = (int)reader["QtyInstock"];
						data.IsExclusive = (string)reader["IsExclusive"];
						data.T1CPointForProduct = (int)reader["T1CPointForProduct"];
						data.IsClearance = (string)reader["IsClearance"];
						data.IsFlashDeal = (string)reader["IsFlashDeal"];
						data.FlashDealStartDate = (DateTime)reader["FlashDealStartDate"];
						data.FlashDealEndDate = (DateTime)reader["FlashDealEndDate"];
						data.ImageURL1 = (string)reader["ImageURL1"];
						data.ImageURL2 = (string)reader["ImageURL2"];
						data.ImageURL3 = (string)reader["ImageURL3"];
						data.ImageURL4 = (string)reader["ImageURL4"];
						data.ImageURL5 = (string)reader["ImageURL5"];
						data.ImageURL6 = (string)reader["ImageURL6"];
						data.ImageURL7 = (string)reader["ImageURL7"];
						data.ImageURL8 = (string)reader["ImageURL8"];
						data.ImageURL9 = (string)reader["ImageURL9"];
						data.ImageURL10 = (string)reader["ImageURL10"];
						data.Type = (string)reader["Type"];
						data.IsStoreExclude = (string)reader["IsStoreExclude"];
						data.IsNotOrder = (string)reader["IsNotOrder"];
						data.DeptNameThaiLevel1 = (string)reader["DeptThaiName"];
						data.DeptNameEngLevel1= (string)reader["DeptNameEngLevel1"];
						data.DeptNameThaiLevel2 = (string)reader["DeptThaiNameLevel2"];
						data.DeptNameEngLevel2= (string)reader["DeptNameEngLevel2"];


						//data.PromoStartdate = (DateTime)reader["PromoStartdate"];
						//data.PromoEnddate = (DateTime)reader["PromoEnddate"];
						//data.CreateOn = (DateTime)reader["CreateOn"];
						//data.CreateBy = (string)reader["CreateBy"];
						//data.UpdateOn = (DateTime)reader["UpdateOn"];
						//data.UpdateBy = (string)reader["UpdateBy"];
						BB.Add(data);
					}
				}
			}
			return BB;
		}

		public List<BuzzyBeeDept> GetBuzzyBEEDept()
		{
			List<BuzzyBeeDept> BB = new List<BuzzyBeeDept>();

			using (SqlCommand command = new SqlCommand(
				@"
							SELECT  * FROM (
						
						SELECT  DISTINCT  	prn.DeptID,prn.DeptThaiName,prn.DeptEngName,
											chn.DeptID as DeptIDLayer2,chn.DeptThaiName as DeptThaiNameLayer2 ,chn.DeptEngName as DeptEngNameLayer2,
											prod.CodeId
						FROM    TBProductCenter prod WITH(NOLOCK)
									INNER JOIN TBDeptStructure ch WITH(NOLOCK) ON ch.DeptId = prod.DeptID
									INNER JOIN TBDeptMaster chn WITH(NOLOCK) ON chn.DeptID = ch.DeptId
									INNER JOIN TBDeptStructure pr WITH(NOLOCK) ON pr.DeptId = ch.ParentDeptId
									INNER JOIN TBDeptMaster prn WITH(NOLOCK) ON prn.DeptID = pr.DeptId 
						WHERE pr.ParentDeptId = 0  AND pr.DeptStatus = 'Active' AND prod.Status NOT IN ('Hold','Delete', 'Clearance') AND prod.pID NOT IN 
						( SELECT ProductId  FROM  TBGiftPremiumGroup) 
						) AS T1
							ORDER BY 
								CASE when T1.DeptID = '16' then 1
								WHEN T1.DeptID  = '1617' then 2
								WHEN T1.DeptID = '22' then 3
								WHEN T1.DeptID = '17' then 4
								WHEN T1.DeptID = '38' then 5
								WHEN T1.DeptID = '18' then 6
								WHEN T1.DeptID = '45' then 7
								WHEN T1.DeptID = '19' then 8
								WHEN T1.DeptID = '20' then 9
								WHEN T1.DeptID = '21' then 10
									ELSE 11
						 end ASC", connection))
			{
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						BuzzyBeeDept data = new BuzzyBeeDept();
						data.DeptID = (int)reader["DeptID"];
						data.DeptThaiName = (string)reader["DeptThaiName"];
						data.DeptEngName = (string)reader["DeptEngName"];
						data.DeptIDLayer2 = (int)reader["DeptIDLayer2"];
						data.DeptThaiNameLayer2 = (string)reader["DeptThaiNameLayer2"];
						data.DeptEngNameLayer2 = (string)reader["DeptEngNameLayer2"];
						data.CodeId = (string)reader["CodeId"];
						BB.Add(data);
					}
				}
			}
			return BB;
		}

		public void UpdateLog(string size)
		{
			using (SqlCommand command = new SqlCommand(@"INSERT INTO TBTransferFileLog
											SELECT 'ImportData','" + size + "',getdate(),'OFM','Complete'", connection))
				command.ExecuteNonQuery();
		}
	}
}
