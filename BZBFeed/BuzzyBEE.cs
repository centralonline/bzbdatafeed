﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductFeedApp
{
	public class BuzzyBEE
	{

		public string pID { get; set; }
		public string CodeId { get; set; }
		public string pTName { get; set; }
		public string pEName { get; set; }
		public string DeptID { get; set; }
		public string CodeID { get; set; }
		public string SubDeptID { get; set; }
		public string pTSDesc { get; set; }
		public string pESDesc { get; set; }
		public string pTLDesc { get; set; }
		public string pELDesc { get; set; }
		public string Status { get; set; }
		public string pTUnit { get; set; }
		public string pEUnit { get; set; }
		public string PromotionTtext { get; set; }
		public string PromotionEtext { get; set; }
		public decimal FullPrice { get; set; }
		public decimal PriceIncVat { get; set; }
		public decimal PriceExcVat { get; set; }
		public string BrandID { get; set; }
		public string BrandName { get; set; }
		public decimal DeliverFee { get; set; }
		public string IsVat { get; set; }
		public string IsBestDeal { get; set; }
		public string IsPremium { get; set; }
		public string pInstallment { get; set; }
		public string IsPromotion { get; set; }
		public DateTime PromoStartdate { get; set; }
		public DateTime PromoEnddate { get; set; }
		public string barcode { get; set; }
		public string Incatalog { get; set; }
		public int QtyInstock { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public DateTime UpdateOn { get; set; }
		public string UpdateBy { get; set; }
		public string ProductURL
		{
			get
			{
				return String.Format("http://www.officemate.co.th/Product/{0}/", pID);
			}
		}
		public string ImageURL
		{
			get
			{
				return String.Format("http://cdn{0}.officemate.co.th/images/lpimage/{1}.jpg", RandomCdn, pID);
			}
		}
		public int RandomCdn
		{
			get
			{
				Random rnd = new Random();
				return rnd.Next(0, 8);
			}
		}


		public string BrandEName { get; set; }
		public string IsExclusive { get; set; }

		public int T1CPointForProduct { get; set; }

		public string IsClearance { get; set; }

		public string IsFlashDeal { get; set; }

		public DateTime FlashDealStartDate { get; set; }

		public DateTime FlashDealEndDate { get; set; }

		public string ImageURL1 { get; set; }
		public string ImageURL2 { get; set; }
		public string ImageURL3 { get; set; }
		public string ImageURL4 { get; set; }
		public string ImageURL5 { get; set; }
		public string ImageURL6 { get; set; }
		public string ImageURL7 { get; set; }
		public string ImageURL8 { get; set; }
		public string ImageURL9 { get; set; }
		public string ImageURL10 { get; set; }
		public string IsNotOrder { get; set; }
		public string IsStoreExclude { get; set; }
		public string DeptNameThaiLevel1 { get; set; }
		public string DeptNameThaiLevel2 { get; set; }
		public string DeptNameEngLevel1 { get; set; }
		public string DeptNameEngLevel2 { get; set; }
		public string Type { get; set; }
	}
}
