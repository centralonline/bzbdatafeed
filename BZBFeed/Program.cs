﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Data.SqlClient;
using ProductFeedApp;
using OfficeMate.Framework.Security.Cryptography;
using System.IO;
using System.Threading;

namespace BZBFeed
{
	class Program
	{
		//UAT
		//private static readonly string ConnectionString = ConnectionStringEncryption.Decrypt("fM3OMOV33x3azgGY4+Gm+Pggrq8cUkIbcRsMAnz9glFjmyaGnc45DipDLHMjLsT/atJkzv9ahdhrXscWzoPEij/A7RvWITJfdtYnc4eoF+bApLRdBhCaiky4hxSOuoWpP8dtijdceOI0pg2Uoa4O1n9p1gyx8H4tBdHEUb1/amAYw7BB+S1POw==");
		
		//Production
		private static readonly string ConnectionString = ConnectionStringEncryption.Decrypt("fM3OMOV33x3azgGY4+Gm+CBusztsCoHkC/TQ87bVyF7DONj5ZIvawk6RTU5lNNkyqUo70SirZUufW63Nq0HH5v+O84YidyfxMYEVS6oM8a9a/3a5PtuYKx1XTp3nfg/qnJzDzBaAtbVAk1Xqt4IHh3rX+tv5CT0hA4JFCs1rhoM=");
		
		private static readonly string PathAsSaveFileName = "ftp://10.17.251.24/html/datafeed/BuzzyBEE/" + "Product.xls";
		//private static readonly string PathAsSaveFileName = "ftp://10.17.251.24/html/datafeed/BuzzyBEE/" + "ProductTest.xls";
		private static readonly string User = "cdn";
		private static readonly string Pwd = "Cdn@123!";
		private static StringBuilder messageAlert = new StringBuilder();
		static void Main(string[] args)
		{
			Console.WriteLine("Start Date: {0}", DateTime.Now);
			messageAlert.AppendLine("BZB Data feed ");
			messageAlert.AppendLine(string.Format("Start Date: {0}", DateTime.Now));
			GenBuzzyBEE();
			//int milliseconds = 90000;
			//Thread.Sleep(milliseconds);
			//long FileSize = Getfilesize(PathAsSaveFileName, User, Pwd);
			//messageAlert.AppendLine(string.Format("File Size: {0}",FileSize));
			//UpdateLog(FileSize);
			Console.WriteLine("End Date: {0}", DateTime.Now);
			messageAlert.AppendLine(string.Format("End Date: {0}", DateTime.Now));
			//SendMessageToLine(messageAlert.ToString());
			SendMessageToLine(string.Format("RunBZB: {0}", DateTime.Now));
			//Console.ReadLine();

		}
		private static void SendMessageToLine(string Message)
		{
			try
			{
				var request = (HttpWebRequest)WebRequest.Create("https://notify-api.line.me/api/notify");
				var postData = string.Format("message={0}", Message);
				string access = string.Format("Bearer {0}", "4IG9KWRvXXIYxbBiJhzERdeYkltfLZQIxbcB9PKXyVs");
				var data = Encoding.UTF8.GetBytes(postData);
				request.Method = "POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = data.Length;
				request.Headers.Add("Authorization", access);

				using (var stream = request.GetRequestStream())
				{
					stream.Write(data, 0, data.Length);
				}

				var response = (HttpWebResponse)request.GetResponse();
			}
			catch (Exception)
			{

				throw;
			}
		}
		private static void GenBuzzyBEE()
		{
			List<BuzzyBEE> BuzzyBEE = new List<ProductFeedApp.BuzzyBEE>();
			List<BuzzyBeeDept> BuzzyBeeDept = new List<BuzzyBeeDept>();
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				connection.Open();
				BuzzyBEE = new DataProductToBuzzyBEERepository(connection).GetBuzzyBEE();
				BuzzyBeeDept = new DataProductToBuzzyBEERepository(connection).GetBuzzyBEEDept();
				connection.Close();
			}
			string s = "";
			byte[] c = new byte[] { 0x1E, 0x1D };
			s = System.Text.ASCIIEncoding.ASCII.GetString(c);



			var workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("ReviewProduct");
			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			cellstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			CellStyle cellstyle_custom = workbook.CreateCellStyle();
			cellstyle_custom.DataFormat = HSSFDataFormat.GetBuiltinFormat("m/d/yy h:mm");

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;

			Cell cell_ProductHeader;
			Row row_DetailProcuctHeader;
			int cell_width = 0;

			#region Create Header
			var rowIndex = 0;
			row_DetailProcuctHeader = sheet.CreateRow(rowIndex);
			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(0);
			cell_ProductHeader.SetCellValue("pID");// [varchar](7) NOT NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(0, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(1);
			cell_ProductHeader.SetCellValue("pTName"); // [varchar](200) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 1000;
			cell_ProductHeader.Sheet.SetColumnWidth(1, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(2);
			cell_ProductHeader.SetCellValue("pEName"); // [varchar](200) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(2, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(3);
			cell_ProductHeader.SetCellValue("DeptID"); // [int] NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 600;
			cell_ProductHeader.Sheet.SetColumnWidth(3, cell_width);

		
			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(4);
			cell_ProductHeader.SetCellValue("SubDeptID"); // [varchar](max) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(4, cell_width);

		

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(5);
			cell_ProductHeader.SetCellValue("pTSDesc"); // [varchar](max) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(5, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(6);
			cell_ProductHeader.SetCellValue("pESDesc"); // [varchar](max) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(6, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(7);
			cell_ProductHeader.SetCellValue("pTLDesc"); // [varchar](max) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(7, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(8);
			cell_ProductHeader.SetCellValue("pELDesc"); // [varchar](max) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(8, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(9);
			cell_ProductHeader.SetCellValue("Status"); // [varchar](10) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(9, cell_width);

			//cell_ProductHeader = row_DetailProcuctHeader.CreateCell(10);
			//cell_ProductHeader.SetCellValue("pTUnit"); // [varchar](10) NULL,
			//cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			//cell_ProductHeader.CellStyle = cellstyle;
			//cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			//cell_ProductHeader.Sheet.SetColumnWidth(10, cell_width);

			//cell_ProductHeader = row_DetailProcuctHeader.CreateCell(11);
			//cell_ProductHeader.SetCellValue("pEUnit"); // [varchar](10) NULL,
			//cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			//cell_ProductHeader.CellStyle = cellstyle;
			//cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			//cell_ProductHeader.Sheet.SetColumnWidth(11, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(12);
			cell_ProductHeader.SetCellValue("PromotionTtext"); // [varchar](10) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(12, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(13);
			cell_ProductHeader.SetCellValue("PromotionEtext"); // [varchar](1000) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(13, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(14);
			cell_ProductHeader.SetCellValue("FullPrice"); // [varchar](1000) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(14, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(15);
			cell_ProductHeader.SetCellValue("PriceIncVat"); //  [decimal](9, 2) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(15, cell_width);

			//cell_ProductHeader = row_DetailProcuctHeader.CreateCell(16);
			//cell_ProductHeader.SetCellValue("PriceExcVat"); //  [decimal](9, 2) NULL,
			//cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			//cell_ProductHeader.CellStyle = cellstyle;
			//cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			//cell_ProductHeader.Sheet.SetColumnWidth(16, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(17);
			cell_ProductHeader.SetCellValue("BrandID"); //  [decimal](9, 2) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(17, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(18);
			cell_ProductHeader.SetCellValue("BrandName"); //  [varchar](5) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(18, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(19);
			cell_ProductHeader.SetCellValue("BrandEName"); //  [varchar](50) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(19, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(20);
			cell_ProductHeader.SetCellValue("DeliverFee"); //  [decimal](9, 2) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(20, cell_width);

			//cell_ProductHeader = row_DetailProcuctHeader.CreateCell(21);
			//cell_ProductHeader.SetCellValue("IsVat"); //  [varchar](3) NULL,
			//cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			//cell_ProductHeader.CellStyle = cellstyle;
			//cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			//cell_ProductHeader.Sheet.SetColumnWidth(21, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(22);
			cell_ProductHeader.SetCellValue("IsBestDeal"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(22, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(23);
			cell_ProductHeader.SetCellValue("IsPremium"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(23, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(24);
			cell_ProductHeader.SetCellValue("pInstallment"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(24, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(25);
			cell_ProductHeader.SetCellValue("IsPromotion"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(25, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(26);
			cell_ProductHeader.SetCellValue("barcode"); //  [varchar](20) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(26, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(27);
			cell_ProductHeader.SetCellValue("Incatalog"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(27, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(28);
			cell_ProductHeader.SetCellValue("QtyInstock"); //  [int] NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(28, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(29);
			cell_ProductHeader.SetCellValue("IsExclusive"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(29, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(30);
			cell_ProductHeader.SetCellValue("T1CPointForProduct"); //  [int] NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(30, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(31);
			cell_ProductHeader.SetCellValue("IsClearance"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(31, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(32);
			cell_ProductHeader.SetCellValue("IsFlashDeal"); //  [varchar](3) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(32, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(33);
			cell_ProductHeader.SetCellValue("FlashDealStartDate"); //  [smalldatetime] NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(33, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(34);
			cell_ProductHeader.SetCellValue("FlashDealEndDate"); //  [smalldatetime] NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(34, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(35);
			cell_ProductHeader.SetCellValue("ImageURL1"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(35, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(36);
			cell_ProductHeader.SetCellValue("ImageURL2"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(36, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(37);
			cell_ProductHeader.SetCellValue("ImageURL3"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(37, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(38);
			cell_ProductHeader.SetCellValue("ImageURL4"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(38, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(39);
			cell_ProductHeader.SetCellValue("ImageURL5"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(39, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(40);
			cell_ProductHeader.SetCellValue("ImageURL6"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(40, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(41);
			cell_ProductHeader.SetCellValue("ImageURL7"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(41, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(42);
			cell_ProductHeader.SetCellValue("ImageURL8"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(42, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(43);
			cell_ProductHeader.SetCellValue("ImageURL9"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(43, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(44);
			cell_ProductHeader.SetCellValue("ImageURL10"); //  [varchar](150) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(44, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(45);
			cell_ProductHeader.SetCellValue("Type"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(45, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(46);
			cell_ProductHeader.SetCellValue("CodeID"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(46, cell_width);


			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(47);
			cell_ProductHeader.SetCellValue("CreateDate"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(47, cell_width);


			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(48);
			cell_ProductHeader.SetCellValue("IsStoreExclude"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(48, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(49);
			cell_ProductHeader.SetCellValue("IsNotOrder"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(49, cell_width);


			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(50);
			cell_ProductHeader.SetCellValue("DeptThaiName"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(50, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(51);
			cell_ProductHeader.SetCellValue("DeptEngName"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(51, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(52);
			cell_ProductHeader.SetCellValue("DeptThaiNameLayer2"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(52, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(53);
			cell_ProductHeader.SetCellValue("DeptEngNameLayer2"); //  [varchar](6) NULL,
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 400;
			cell_ProductHeader.Sheet.SetColumnWidth(53, cell_width);
			#endregion

			rowIndex++;
			Cell cell_ProductDetail;
			Row row_DetailProduct;

			DateTime nowCreateFile = DateTime.Now;

			foreach (var item in BuzzyBEE)
			{
				if (!string.IsNullOrEmpty(item.DeptNameEngLevel1) && item.DeptNameEngLevel2 != "-")
				{
					#region fill each cell
					row_DetailProduct = sheet.CreateRow(rowIndex);

					cell_ProductDetail = row_DetailProduct.CreateCell(0);
					cell_ProductDetail.SetCellValue(item.pID);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(1);
					cell_ProductDetail.SetCellValue(item.pTName);
					cell_ProductDetail.CellStyle = cellstyle_detail;

					cell_ProductDetail = row_DetailProduct.CreateCell(2);
					cell_ProductDetail.SetCellValue(item.pEName);
					cell_ProductDetail.CellStyle = cellstyle_detail;

					cell_ProductDetail = row_DetailProduct.CreateCell(3);
					cell_ProductDetail.SetCellValue(item.DeptID);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(4);
					cell_ProductDetail.SetCellValue(item.SubDeptID);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(5);
					cell_ProductDetail.SetCellValue(item.pTSDesc);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(6);
					cell_ProductDetail.SetCellValue(item.pESDesc);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(7);
					cell_ProductDetail.SetCellValue(item.pTLDesc);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(8);
					cell_ProductDetail.SetCellValue(item.pELDesc);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(9);
					cell_ProductDetail.SetCellValue(item.Status);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(10);
					cell_ProductDetail.SetCellValue(item.pTUnit);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(11);
					cell_ProductDetail.SetCellValue(item.pEUnit);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(12);
					cell_ProductDetail.SetCellValue(item.PromotionTtext);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(13);
					cell_ProductDetail.SetCellValue(item.PromotionEtext);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(14);
					cell_ProductDetail.SetCellValue(item.FullPrice.ToString("N2"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(15);
					cell_ProductDetail.SetCellValue(item.PriceIncVat.ToString("N2"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(16);
					cell_ProductDetail.SetCellValue(item.PriceExcVat.ToString("N2"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(17);
					cell_ProductDetail.SetCellValue(item.BrandID);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(18);
					cell_ProductDetail.SetCellValue(item.BrandName);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(19);
					cell_ProductDetail.SetCellValue(item.BrandEName);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(20);
					cell_ProductDetail.SetCellValue(item.DeliverFee.ToString("N0"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(21);
					cell_ProductDetail.SetCellValue(item.IsVat);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(22);
					cell_ProductDetail.SetCellValue(item.IsBestDeal.ToString());
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(23);
					cell_ProductDetail.SetCellValue(item.IsPremium.ToString());
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(24);
					cell_ProductDetail.SetCellValue(item.pInstallment.ToString());
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(25);
					cell_ProductDetail.SetCellValue(item.IsPromotion.ToString());
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(26);
					cell_ProductDetail.SetCellValue(item.barcode);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(27);
					cell_ProductDetail.SetCellValue(item.Incatalog);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(28);
					cell_ProductDetail.SetCellValue(item.QtyInstock);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(29);
					cell_ProductDetail.SetCellValue(item.IsExclusive);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(30);
					cell_ProductDetail.SetCellValue(item.T1CPointForProduct);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(31);
					cell_ProductDetail.SetCellValue(item.IsClearance);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(32);
					cell_ProductDetail.SetCellValue(item.IsFlashDeal);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					//if (DateTime.Compare(item.FlashDealStartDate, new DateTime(1900, 1, 1, 0, 0, 0)) == 0)
					//{
					//    cell_ProductDetail = row_DetailProduct.CreateCell(33);
					//    cell_ProductDetail.SetCellValue("-");
					//    cell_ProductDetail.CellStyle = cellstyle_detail;
					//    cell_ProductDetail.CellStyle = cellstyle;
					//}
					//else
					//{
					//    cell_ProductDetail = row_DetailProduct.CreateCell(33);
					//    cell_ProductDetail.SetCellValue(item.FlashDealStartDate.ToString("d/M/yyyy H:mm"));
					//    cell_ProductDetail.CellStyle = cellstyle_detail;
					//    cell_ProductDetail.CellStyle = cellstyle;
					//}

					//if (DateTime.Compare(item.FlashDealEndDate, new DateTime(1900, 1, 1, 0, 0, 0)) == 0)
					//{
					//    cell_ProductDetail = row_DetailProduct.CreateCell(34);
					//    cell_ProductDetail.SetCellValue("-");
					//    cell_ProductDetail.CellStyle = cellstyle_detail;
					//    cell_ProductDetail.CellStyle = cellstyle;
					//}
					//else
					//{
					//    cell_ProductDetail = row_DetailProduct.CreateCell(34);
					//    cell_ProductDetail.SetCellValue(item.FlashDealEndDate.ToString("d/M/yyyy H:mm"));
					//    cell_ProductDetail.CellStyle = cellstyle_detail;
					//    cell_ProductDetail.CellStyle = cellstyle;
					//}

					cell_ProductDetail = row_DetailProduct.CreateCell(33);
					cell_ProductDetail.SetCellValue(item.FlashDealStartDate.ToString("d/M/yyyy H:mm"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle_custom;

					cell_ProductDetail = row_DetailProduct.CreateCell(34);
					cell_ProductDetail.SetCellValue(item.FlashDealEndDate.ToString("d/M/yyyy H:mm"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle_custom;

					cell_ProductDetail = row_DetailProduct.CreateCell(35);
					cell_ProductDetail.SetCellValue(item.ImageURL1);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(36);
					cell_ProductDetail.SetCellValue(item.ImageURL2);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(37);
					cell_ProductDetail.SetCellValue(item.ImageURL3);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(38);
					cell_ProductDetail.SetCellValue(item.ImageURL4);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(39);
					cell_ProductDetail.SetCellValue(item.ImageURL5);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(40);
					cell_ProductDetail.SetCellValue(item.ImageURL6);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(41);
					cell_ProductDetail.SetCellValue(item.ImageURL7);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(42);
					cell_ProductDetail.SetCellValue(item.ImageURL8);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(43);
					cell_ProductDetail.SetCellValue(item.ImageURL9);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;
					cell_ProductDetail = row_DetailProduct.CreateCell(44);
					cell_ProductDetail.SetCellValue(item.ImageURL10);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(45);
					cell_ProductDetail.SetCellValue(item.Type);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(46);
					cell_ProductDetail.SetCellValue(item.CodeId);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(47);
					cell_ProductDetail.SetCellValue(nowCreateFile.ToString("dd/MM/yyyy"));
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(48);
					cell_ProductDetail.SetCellValue(item.IsStoreExclude);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(49);
					cell_ProductDetail.SetCellValue(item.IsNotOrder);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(50);
					cell_ProductDetail.SetCellValue(item.DeptNameThaiLevel2);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(51);
					cell_ProductDetail.SetCellValue(item.DeptNameEngLevel2);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(52);
					cell_ProductDetail.SetCellValue(item.DeptNameThaiLevel1);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;

					cell_ProductDetail = row_DetailProduct.CreateCell(53);
					cell_ProductDetail.SetCellValue(item.DeptNameEngLevel1);
					cell_ProductDetail.CellStyle = cellstyle_detail;
					cell_ProductDetail.CellStyle = cellstyle;


					rowIndex++;
					#endregion
				}
			}

			

			FtpWebRequest request = (FtpWebRequest)WebRequest.Create(PathAsSaveFileName);

			request.Method = WebRequestMethods.Ftp.UploadFile;
			request.UsePassive = false;
			request.Credentials = new NetworkCredential(User, Pwd);

			if (CheckIfFileExistsOnServer(PathAsSaveFileName, User, Pwd/*, request*/))
				DeleteFTPFile(PathAsSaveFileName, User, Pwd/*, request*/);

			Stream ss = request.GetRequestStream();

			workbook.Write(ss);
			ss.Close();

		

		
			FtpWebResponse response = (FtpWebResponse)request.GetResponse();
			if (response.StatusCode == FtpStatusCode.ClosingData)
			{
				//long FileSize = Getfilesize(PathAsSaveFileName, User, Pwd);
				//ListBox.Items.Add("[File] BuzzyBEEFeeds : Upload File Successful"); ListBox.Items.Add("");
				//ListBox.Items.Add("[File] BuzzyBEEFeeds Size : " + x); ListBox.Items.Add("");
				//MessageBox.Show("Created SuccesFully!");
			}
			else
			{
				//ListBox.Items.Add("[Error] BuzzyBEEFeeds : Error uploading file : " + response.StatusDescription); ListBox.Items.Add("");
				//ListBox.Items.Add("[File] BuzzyBEEFeeds Size : " + x); ListBox.Items.Add("");
				//MessageBox.Show("Error uploading file:" + response.StatusDescription);
			}

			response.Close();
		}
		private static long Getfilesize(string LocalFilePath, string User, string Pwd)
		{
			FtpWebRequest Req = (FtpWebRequest)WebRequest.Create(LocalFilePath);
			Req.Credentials = new NetworkCredential(User, Pwd);
			Req.Method = WebRequestMethods.Ftp.GetFileSize;

			FtpWebResponse response = (FtpWebResponse)Req.GetResponse();

			return response.ContentLength;
		}
		private static bool CheckIfFileExistsOnServer(string LocalFilePath, string User, string Pwd /*FtpWebRequest request*/)
		{
			FtpWebRequest Req = (FtpWebRequest)WebRequest.Create(LocalFilePath);
			Req.Credentials = new NetworkCredential(User, Pwd);
			Req.Method = WebRequestMethods.Ftp.GetFileSize;
			try
			{
				FtpWebResponse response = (FtpWebResponse)Req.GetResponse();
				response.Close();
				return true;
			}
			catch (WebException ex)
			{
				FtpWebResponse response = (FtpWebResponse)ex.Response;
				if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
					response.Close();
				return false;
			}
		}
		private static void DeleteFTPFile(string LocalFilePath, string User, string Pwd/*, FtpWebRequest clsRequest*/)
		{
			FtpWebRequest clsRequest = (FtpWebRequest)WebRequest.Create(LocalFilePath);
			clsRequest.Credentials = new NetworkCredential(User, Pwd);

			clsRequest.Method = WebRequestMethods.Ftp.DeleteFile;

			string result = string.Empty;
			FtpWebResponse response = (FtpWebResponse)clsRequest.GetResponse();
			response.Close();

		}
		public static string ToFileSize(double value)
		{
			string[] suffixes = { "bytes", "KB", "MB", "GB",
        "TB", "PB", "EB", "ZB", "YB"};
			for (int i = 0; i < suffixes.Length; i++)
			{
				if (value <= (Math.Pow(1024, i + 1)))
				{
					return ThreeNonZeroDigits(value /
						Math.Pow(1024, i)) +
						" " + suffixes[i];
				}
			}

			return ThreeNonZeroDigits(value /
				Math.Pow(1024, suffixes.Length - 1)) +
				" " + suffixes[suffixes.Length - 1];
		}
		private static string ThreeNonZeroDigits(double value)
		{
			if (value >= 100)
			{
				// No digits after the decimal.
				return value.ToString("0,0");
			}
			else if (value >= 10)
			{
				// One digit after the decimal.
				return value.ToString("0.0");
			}
			else
			{
				// Two digits after the decimal.
				return value.ToString("0.00");
			}
		}
		private static void UpdateLog(long FileSize)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				connection.Open();
				new DataProductToBuzzyBEERepository(connection).UpdateLog(ToFileSize(FileSize));
				connection.Close();
			}
		}
	}
}
